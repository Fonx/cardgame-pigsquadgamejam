using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveHorizontal : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(MoveHorizontalRotine());
    }

    IEnumerator MoveHorizontalRotine() {
        WaitForSeconds w8 = new WaitForSeconds(1/60f);
        Vector3 initialPos = transform.position;
        Vector3 finalPos = transform.position + Vector3.left * 20;
        while (true) {
            while (transform.position != finalPos)
            {
                yield return w8;
                transform.position = Vector3.MoveTowards(transform.position, finalPos, Time.deltaTime * 30f);
            }
            while (transform.position != initialPos)
            {
                yield return w8;
                transform.position = Vector3.MoveTowards(transform.position, initialPos, Time.deltaTime * 30f);
            }
        }

    }
}
