using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class ToolTip : SubscriberSound
{
    public static ToolTip _instance;
    public TMP_Text textToShow;
    public Image border;
    public Image symbol;

    //private Queue<Task> tasks = new Queue<Task>();
    private Coroutine currentRoutine;

    private Color finalColor = Color.white;
    private Color initialColor = Color.clear;
    public int speed = 150;

    public AudioClip audio1;
    public AudioClip audio2;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;

        else
            Destroy(gameObject);
    }

    public void ShowMessage(string text) {
        //tasks.Enqueue(new Task(text));
        textToShow.text = text;
        if (currentRoutine != null)
            StopCoroutine(currentRoutine);

        currentRoutine = StartCoroutine(ShowMsgRoutine());
    }

    IEnumerator ShowMsgRoutine()
    {
        WaitForSeconds w8 = new WaitForSeconds(1 / 60);
        border.gameObject.SetActive(true);

        border.color = initialColor;
        textToShow.color = initialColor;
        symbol.color = initialColor;

        audioSource.PlayOneShot(audio1);

        //InitializeValues(tasks.Peek());

        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime * speed;
            border.color = Color.Lerp(initialColor, finalColor, t);
            textToShow.color = Color.Lerp(initialColor, finalColor, t);
            symbol.color = Color.Lerp(initialColor, finalColor, t);
            yield return w8;
        }

        yield return new WaitForSeconds(1.25f);
        audioSource.PlayOneShot(audio2);

        t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime * speed;
            border.color = Color.Lerp(finalColor, initialColor, t);
            textToShow.color = Color.Lerp(finalColor, initialColor, t);
            symbol.color = Color.Lerp(finalColor, initialColor, t);
            yield return w8;
        }
        border.gameObject.SetActive(false);
    }

/*    private void InitializeValues(Task task)
    {
        textToShow.text = task.text;
    }*/

}

/*struct Task
{
    public string text; 
    public Task(string text)
    {
        this.text = text;
    }
}
*/