using UnityEngine;

public class SubscriberSound : MonoBehaviour
{
    private SettingsMenu targetSettings;
    protected AudioSource audioSource;
    protected virtual void Start()
    {
        targetSettings = SettingsMenu._instance;
        if (targetSettings != null)
        {
            targetSettings.onSoundChange += onMusicChange;
        }

        audioSource = GetComponent<AudioSource>();
        if (PlayerPrefs.GetInt("VolumeSaved") == 1)
            audioSource.volume = PlayerPrefs.GetFloat("VolumeSound");

    }

    public void onMusicChange(float newValue)
    {
        if (audioSource != null)
        {
            audioSource.volume = newValue;
        }
    }
}
