using System.Collections;
using UnityEngine;

public class SubscriberBackgroundMusic : MonoBehaviour
{
    private static float TIME_TO_FADE = 0.25f;

    public static SubscriberBackgroundMusic _Instance;

    public AudioSource audioSource;

    public AudioClip startSceneAudioClip;

    private void Awake()
    {
        if (_Instance == null)
        {
            _Instance = this;
        }
        else {
            Destroy(gameObject);
        }
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        SettingsMenu target = SettingsMenu._instance;
        target.onMusicChange += onVolumeChange;
        target.onAllChange += onVolumeChange;

        audioSource.volume = PlayerPrefs.GetFloat("VolumeMusic", 1);
        audioSource.Play();
    }

    public void FadeIntroSongOut()
    {
        StartCoroutine(FadeIntroSongOutEnumerator());
    }

    private IEnumerator FadeIntroSongOutEnumerator()
    {
        float timeElapsed = 0;
        float initVolume = audioSource.volume;
        //Debug.Log("Init volume = "+audioSource.volume);

        while (timeElapsed < TIME_TO_FADE)
        {
            audioSource.volume = Mathf.Lerp(initVolume, 0, timeElapsed / TIME_TO_FADE);

            timeElapsed += Time.deltaTime;
            yield return null;
        }

        audioSource.Stop();
    }

    public void onVolumeChange(float newValue)
    {
        float volAll = PlayerPrefs.GetFloat("VolumeAll", 1);
        float volMusic = PlayerPrefs.GetFloat("VolumeMusic", 1);
        if (audioSource != null)
        {
            audioSource.volume = volAll * volMusic;
        }
    }
}
