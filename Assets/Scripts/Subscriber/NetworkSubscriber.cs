using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkSubscriber : MonoBehaviour
{
    public static NetworkSubscriber _instance;

    private PhotonView PV;
    private const float phaseTime = 8f;

    public event Action<Player> onLocalPlayerConnect = delegate { };
    public event Action<Player> onRivalPlayerConnect = delegate { };
    //public event Action onLoseGame = delegate { };

    public event Action<int> onPhaseChange = delegate { };

    private int currentPhase;
    private List<GamePhase> phases = new List<GamePhase>() { GamePhase.Uppkeep, GamePhase.Management, GamePhase.Combat };

    public int CurrentPhase { get => currentPhase; set => currentPhase = value; }

    [SerializeField] OponentZone oponentZone;

    IEnumerator SlowUpdate() {
        WaitForSeconds w8 = new WaitForSeconds(phaseTime);

        while (true)
        {
            yield return w8;
            currentPhase++;
            if (currentPhase >= phases.Count) {
                currentPhase = 0;
            }
            onPhaseChange.Invoke(currentPhase);
            Debug.Log("change phase");
            PV.RPC("RPCChangePhase", RpcTarget.OthersBuffered, currentPhase);
        }
    }

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(gameObject);

        PV = GetComponent<PhotonView>();
    }

    void Start()
    {
        onLocalPlayerConnect.Invoke(PhotonNetwork.LocalPlayer);
        Debug.Log(PhotonNetwork.LocalPlayer.NickName);
        PV.RPC("RPCSendMyName", RpcTarget.OthersBuffered, PhotonNetwork.LocalPlayer);
        if (PhotonNetwork.IsMasterClient) {
            StartCoroutine(SlowUpdate());
        }
    }

    public void Lose() {
        StopAllCoroutines();
        PV.RPC("RPCWinGame", RpcTarget.Others);
    }

    public void ShowMyAttackers(List<CardPosition> cardsPosition)
    {
        int[] cardsElement = new int[cardsPosition.Count];
        int[] cardsPos = new int[cardsPosition.Count];

        for (int i = 0; i < cardsPosition.Count; i++)
        {
            cardsElement[i] = cardsPosition[i].Card.id;
            cardsPos[i] = cardsPosition[i].Position;
        }

        PV.RPC("RPCShowOponentAttackers", RpcTarget.Others, cardsElement, cardsPos);
    }

    public void PlayMagicCard(string keycode, int id) {
        PV.RPC("RPCMagicCard", RpcTarget.Others, keycode, id);
    }
    public void RivalPlayerConnect(Player player) 
    {
        onRivalPlayerConnect.Invoke(player);
    }

    #region RPCS

    [PunRPC]
    void RPCSendMyName(Player player)
    {
        onRivalPlayerConnect.Invoke(player);
    }

    [PunRPC]
    void RPCChangePhase(int currentPhase)
    {
        onPhaseChange.Invoke(currentPhase);
    }

    [PunRPC]
    void RPCShowOponentAttackers(int[] cardsElement, int[] cardsPos)
    {
        List<CardPosition> cardsPosition = new List<CardPosition>();
        GameManager manager = GameManager._instance;

        for (int i=0;i<cardsElement.Length;i++) {
            CardPosition newCard = new CardPosition(manager.GetCard(cardsElement[i]), cardsPos[i]);
            Debug.Log(newCard);
            cardsPosition.Add(newCard);
        }
        oponentZone.InstantiateOponentCards(cardsPosition);
    }

    [PunRPC]
    void RPCWinGame()
    {
        GameManager._instance.WinGame();
        StopAllCoroutines();
    }    
    
    [PunRPC]
    void RPCMagicCard(string keycode,int id)
    {
        GameManager._instance.PlayCardAnimation(id);
        Instantiate(Resources.Load($"Prefabs/Effects/{keycode}"));
    }

    #endregion


}

public class CardPosition
{
    private CardSO card;
    private int position;

    public CardPosition(CardSO card, int position)
    {
        this.card = card;
        this.position = position;
    }

    public CardSO Card { get => card; set => card = value; }
    public int Position { get => position; set => position = value; }
}