using UnityEngine;
using System;

public class CameraRaycaster : MonoBehaviour
{
    public LayerMask layerMask;

    public event Action<GameObject> onMouseDown = delegate { };
    public event Action<CardPlacer, GameObject> onMouseUp = delegate { };
    public event Action<Card> onMouseHover = delegate { };
    public event Action<Card> onChooserHover = delegate { };
    public event Action onMouseEndHover = delegate { };
    public event Action onHouveBack = delegate { };
    //public event Action<CardDeck> onPickCard = delegate { };

    private GameObject currentDrag;
    private Collider currentSeen;

    public GameObject CurrentDrag { get => currentDrag; set => currentDrag = value; }

    private bool cantDrag = false;

    protected virtual void Start()
    {
        NetworkSubscriber._instance.onPhaseChange += OnPhaseChage;
    }

    private void OnPhaseChage(int phase) {
        if (phase == 2) {
            cantDrag = true;
        }
        else {
            cantDrag = false;
        }
    }

    protected virtual void Update()
    {
        RaycastHit hit;
/*        if (cantDrag) {
            ToolTip._instance.ShowMessage("Can't play cards in the attack phase.");
            return;
        }*/
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, layerMask) && currentDrag == null)
            {
                if (hit.collider.CompareTag("Card") && !cantDrag) {
                    currentDrag = hit.collider.gameObject;
                    onMouseDown?.Invoke(currentDrag);
                }
                else if (hit.collider.CompareTag("Hand")) {
                    onMouseDown?.Invoke(hit.collider.gameObject);
                }               
                else if (hit.collider.CompareTag("Choose")) {
                    onMouseDown?.Invoke(hit.collider.gameObject);
                }
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, layerMask) && !cantDrag)
            {
                if (hit.collider.CompareTag("Placer") && currentDrag != null)
                {
                    CardPlacer thisZone = hit.collider.GetComponent<CardPlacer>();
                    onMouseUp?.Invoke(thisZone, currentDrag);
                    currentDrag = null;
                }
            }
            else if (currentDrag != null || cantDrag)
            {
                onMouseUp?.Invoke(null, currentDrag);
                currentDrag = null;
            }
        }
        else
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, layerMask))
            {
                if ((hit.collider.CompareTag("Card") || hit.collider.CompareTag("Hand")) && currentDrag == null)
                {
                    if (currentSeen != hit.collider) {
                        currentSeen = hit.collider;
                        onMouseHover.Invoke(hit.collider.GetComponent<Card>());
                    }
                }
                else if (hit.collider.CompareTag("Deck") && currentDrag == null)
                {
                    onHouveBack.Invoke();
                }
                else if (hit.collider.CompareTag("Choose")) {
                    onChooserHover.Invoke(hit.collider.GetComponent<Card>());
                }
                else {
                    onMouseEndHover.Invoke();
                    currentSeen = null;
                }
            }
        }
    }

}