using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    public Slider musicSlider;
    public Slider soundSlider;
    public Slider allSlider;

    public Camera mainCamera;
    public AudioSource sound1;
    public AudioSource sound2;
    public AudioSource sound3;///For music

    public event Action<float> onAllChange = delegate { };
    public event Action<float> onSoundChange = delegate { };
    public event Action<float> onMusicChange = delegate { };
    public static SettingsMenu _instance;

    public AudioClip closeClip;

    public GameObject panel;

    public KeyCode escCode = KeyCode.Escape;


    private bool opened = false;
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Start()
    {

        if (PlayerPrefs.GetInt("AllSaved") == 1)
        {
            allSlider.value = PlayerPrefs.GetFloat("VolumeAll");
        }
        if (PlayerPrefs.GetInt("VolumeSaved") == 1)
        {
            soundSlider.value = PlayerPrefs.GetFloat("VolumeSound");
        }
        if (PlayerPrefs.GetInt("MusicSaved") == 1)
        {
            musicSlider.value = PlayerPrefs.GetFloat("VolumeMusic");
        }

        else
        {
            if (mainCamera != null)
            {
                mainCamera.fieldOfView = 38;
            }
        }
    }

    public void OpenPanel() {
        panel.SetActive(true);
        Pause();
    }

    private void Pause() { GameManager._instance.PauseGame(); }

    public void ClosePanel() {
        GameManager._instance.ResumeGame();
        panel.GetComponent<IPanel>().ClosePanel();
    }

    public void PlayCloseClip()
    {
        sound1.PlayOneShot(closeClip);
    }

    public void SlideSound()
    {
        SaveVolumeSound();
    }

    public void SlideAll()
    {
        AudioListener.volume = allSlider.value;
        SaveVolumeAll();
    }

    public void SlideMusicOnly()
    {
        SaveMusicOnly();
    }

    private void SaveMusicOnly()
    {
        PlayerPrefs.SetFloat("VolumeMusic", musicSlider.value);
        PlayerPrefs.SetInt("MusicSaved", 1);
        onMusicChange.Invoke(musicSlider.value);
    }

    private void SaveVolumeAll()
    {
        PlayerPrefs.SetFloat("VolumeAll", allSlider.value);
        PlayerPrefs.SetInt("AllSaved", 1);
        onAllChange.Invoke(allSlider.value);
    }

    private void SaveVolumeSound()
    {
        PlayerPrefs.SetFloat("VolumeSound", soundSlider.value);
        PlayerPrefs.SetInt("VolumeSaved", 1);
        onSoundChange.Invoke(soundSlider.value);
    }

    private void Update()
    {
        if (Input.GetKeyDown(escCode)) {
            if (!opened)
            {
                OpenPanel();
                opened = true;
            }
            else {
                ClosePanel();
                opened = false;
            }
        }
    }

}
