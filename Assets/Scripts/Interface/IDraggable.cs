using UnityEngine;

public interface IDraggable
{
    void OnStartDrag();
    void OnEndDrag(Transform a);
}
