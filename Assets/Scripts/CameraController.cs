using UnityEngine;

public class CameraController : CameraRaycaster
{
    //public Transform target;
    public float rotateSpeed = .2f;
    public float maxVerticalAngle = 1f;
    public float minVerticalAngle = -1f;

    private float mouseX = 0f;
    private float mouseY = 0f;

    private bool paused = false;

    public bool Paused { get => paused; set => paused = value; }

    protected override void Start()
    {
        base.Start();
        GameManager._instance.pause += OnPauseApplication;
    }

    protected override void Update()
    {
        if (paused)
        {
            return;
        }

        base.Update();
        mouseX += Input.GetAxis("Mouse X") * rotateSpeed;
        mouseY -= Input.GetAxis("Mouse Y") * rotateSpeed;
        mouseY = Mathf.Clamp(mouseY, minVerticalAngle, maxVerticalAngle);
        mouseX = Mathf.Clamp(mouseX, minVerticalAngle, maxVerticalAngle);

        transform.eulerAngles = new Vector3(mouseY, mouseX, 0);
        //transform.position = target.position - transform.forward * Vector3.Distance(transform.position, target.position);
    }

    private void OnPauseApplication(bool paused) {
        Debug.Log("OnPauseApplication" + paused);
        this.paused = paused;
    }
}
