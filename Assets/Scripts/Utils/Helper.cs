using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public static class Helper
{
    public static Object[] Shuffle(Object[] arr)
    {
        System.Random rnd = new System.Random();
        return arr.OrderBy(x => rnd.Next()).ToArray();
    }
    public static List<SkillCode> MapCode(List<CollectsSO> collects)
    {
        List<SkillCode> toReturn = new List<SkillCode>();
        for (int i=0;i<collects.Count;i++) {
            toReturn.Add(collects[i].code);
        }
        return toReturn;
    }
}