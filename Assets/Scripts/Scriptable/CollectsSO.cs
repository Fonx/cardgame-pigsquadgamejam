using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/CollectSkill", order = 2)]
public class CollectsSO : ScriptableObject
{
    public string skillName;
    [TextArea]
    public string description;
    public SkillCode code;
    public GameObject effect;


}


//applies on end of phase