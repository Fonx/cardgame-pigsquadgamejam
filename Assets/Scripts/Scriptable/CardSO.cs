using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Card", order = 1)]
public class CardSO : ScriptableObject
{
    public int id;
    public Sprite sprite;
    public string cardName;
    [TextArea]
    public string description;
    //public List<(CardResources, int)> cost;
    public List<CardResource> costResources = new List<CardResource>();
    public List<int> costQtd = new List<int>();
    public CardCategory category;
    //public List<(Skills, int)> skills;
    public List<CollectsSO> collectskills = new List<CollectsSO>();
    public List<int> collectQtd = new List<int>();

    public List<CombatsSO> combatskills = new List<CombatsSO>();

    public List<int> combatQtd = new List<int>();
    public AudioClip clip;

    public Vector2 Status;
    public GameObject effect;

    public string keycode;
}

public enum CardResource { wood, stone,rabbit, gold }
public enum CardCategory { Enviroment, Invocation, Construction, Expansion, Magic, Exchange }
public enum CardZone { EnvorimentZone, CombatZone, MainZone}
//public enum Skills { Explore, StarveRabbit, StarveFrog}

public enum GamePhase { Uppkeep, Management, Combat}