using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/CombatSkill", order = 2)]
public class CombatsSO : ScriptableObject
{
    public string skillName;
    [TextArea]
    public string description;
    //public SkillCode code;
    public GameObject effect;
}
//applies on start of turn