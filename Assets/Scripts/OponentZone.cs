using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OponentZone : SubscriberSound
{
    //[SerializeField] GameObject hideCards;
    [SerializeField] Zone atkZone;
    [SerializeField] Zone mainZone;

    [SerializeField] List<CardPlacer> placersRival = new List<CardPlacer>();
    [SerializeField] List<CardPlacer> placersMine = new List<CardPlacer>();

    [SerializeField] List<CardOponent> oponentCardsInBoard = new List<CardOponent>();


    protected override void Start()
    {
        base.Start();
        NetworkSubscriber._instance.onPhaseChange += OnPhaseChange;
    }

    private void OnPhaseChange(int phase)
    {
        if (phase == 2)
        {
            StartCoroutine(AttackPhaseRotine());
        }
    }

    IEnumerator RangedAtk(CardPlacer attacker, CardPlacer defender, GameObject effectPrefab, int counter)
    {
        if (defender == null || defender.Slot == null) {
            Debug.Log("sem defender");
            yield break;
        }
        WaitForSeconds w8 = new WaitForSeconds(1 / 60f);
        Instantiate(Resources.Load("Prefabs/Effects/CFX_Hit_Misc"), attacker.transform);
        yield return new WaitForSeconds(.4f);
        Instantiate(effectPrefab, defender.transform);
        Damageble[] damageble = effectPrefab.GetComponentsInChildren<Damageble>();
        Debug.Log("lenght" + damageble.Length);
        Debug.Log("counter" + counter);
        for (int i=0;i<damageble.Length;i++) {
            if (damageble[i] != null)
                damageble[i].Initialize(attacker.Slot, counter);
        }

        yield return w8;
    }

    IEnumerator Attack(CardPlacer attacker, CardPlacer defender) {
        Debug.Log("attack");
        WaitForSeconds w8 = new WaitForSeconds(1 / 60f);
        Transform atkTransform = attacker.Slot.transform;
        Transform defTransform = defender.Slot.transform;
        yield return StartCoroutine(AttackAnim(w8, atkTransform, defTransform));
        yield return StartCoroutine(AttackAnim(w8, defTransform, atkTransform));
        if (attacker.Slot.Reference.Status.x > defender.Slot.Reference.Status.y) {
            //mata
            Card selectedCard = defender.Slot;

            attacker.Slot.PutCounter((int)defender.Slot.Reference.Status.y);
            selectedCard.DestroyCard();
            //defender.RemoveFromZone(selectedCard);
            //Destroy(selectedCard.gameObject);
            yield return StartCoroutine(Move(attacker, defender));
            
        }
        else if (attacker.Slot.Reference.Status.x < defender.Slot.Reference.Status.y)
        {
            //morre
            Card selectedCard = attacker.Slot;
            selectedCard.DestroyCard();
        }
        else
        {
            //morre os 2
            Card selectedCardA = defender.Slot;
            selectedCardA.DestroyCard();

            Card selectedCardB = attacker.Slot;
            selectedCardB.DestroyCard();
        }
        yield return w8;
    }
    IEnumerator AttackAnim(WaitForSeconds w8, Transform atk, Transform def)
    {
        Debug.Log("AttackAnim");
        Vector3 initialPosition = atk.position;
        Vector3 finalPosition = def.position;
        while (atk.position != finalPosition)
        {
            yield return w8;
            atk.position = Vector3.MoveTowards(atk.position, finalPosition, Time.deltaTime * 75);
        }
        yield return w8;
        //instantiate Effect;
        Instantiate(Resources.Load("Prefabs/Effects/CFX_Hit_Random"), def);
        while (atk.position != initialPosition)
        {
            yield return w8;
            atk.position = Vector3.MoveTowards(atk.position, initialPosition, Time.deltaTime * 75);
        }
        Instantiate(Resources.Load("Prefabs/Effects/CFX_Hit_Random"), atk);
    }

    IEnumerator Move(CardPlacer current, CardPlacer next) {
        Debug.Log("Move");
        WaitForSeconds w8 = new WaitForSeconds(1 / 60f);
        Transform currentTransform = current.Slot.transform;
        Transform nextTransform = next.transform;
        while (currentTransform.position != nextTransform.position)
        {
            yield return w8;
            currentTransform.position = Vector3.MoveTowards(currentTransform.position, nextTransform.position, Time.deltaTime * 75);
        }
        Card selectedCard = current.Slot;
        current.RemoveFromZone(selectedCard);
        next.PutInZone(selectedCard);
        yield return w8;
    }
    IEnumerator AttackPhaseRotine() {
        Debug.Log("AttackPhaseRotine");
        WaitForSeconds w8 = new WaitForSeconds(1/60f); 
        yield return new WaitForSeconds(.7f);
        Debug.Log(placersRival.Count);
        //mudar
        oponentCardsInBoard.RemoveAll(e => e == null);
        for (int i=0;i< oponentCardsInBoard.Count;i++) {
            yield return w8;
            CardPlacer placerOfThis = oponentCardsInBoard[i].CurrentPlacer;
            if (placerOfThis != null) {
                if (oponentCardsInBoard[i].CurrentPlacer.Next == null) {
                    GameManager._instance.LoseGame();
                    yield break;
                }
                if (placerOfThis.Next.Slot != null)
                {
                    yield return StartCoroutine(Attack(placerOfThis, placerOfThis.Next));
                    yield return w8;
                }
                else {
                    yield return StartCoroutine(Move(placerOfThis, placerOfThis.Next));
                    yield return w8;
                }
            }
        }
        List<CardPlacer> placersOfMainZone = mainZone.Placers;

        for (int i=0;i< placersOfMainZone.Count;i++) {
            if (placersOfMainZone[i].Slot != null) {
                for (int j = 0; j < placersOfMainZone[i].Slot.Reference.combatskills.Count; j++)
                {
                    yield return StartCoroutine(RangedAtk(placersOfMainZone[i], GetRandomEnemy(), placersOfMainZone[i].Slot.Reference.combatskills[j].effect, placersOfMainZone[i].Slot.Reference.combatQtd[j]));
                    yield return w8;
                }
            }
        }

        for (int i = 0; i < oponentCardsInBoard.Count; i++)
        {
            yield return w8;
            CardPlacer placerOfThis = oponentCardsInBoard[i].CurrentPlacer;
            if (placerOfThis != null)
            {
                for (int j = 0; j < oponentCardsInBoard[i].Reference.combatskills.Count; j++)
                {
                    yield return StartCoroutine(RangedAtk(oponentCardsInBoard[i].CurrentPlacer, GetRandomAlly(), oponentCardsInBoard[i].Reference.combatskills[j].effect, oponentCardsInBoard[i].Reference.combatQtd[j]));
                    yield return w8;
                }
            }
        }


        NotifyAttackersToNetwork();

    }

    private CardPlacer GetRandomEnemy() {
        if (oponentCardsInBoard.Count == 0)
            return null;
        int random = Random.Range(0, oponentCardsInBoard.Count);
        return oponentCardsInBoard[random].CurrentPlacer;
    }
    private CardPlacer GetRandomAlly()
    {
        return GameManager._instance.GetRandomAlly();
    }


    private void NotifyAttackersToNetwork() {

        NetworkSubscriber._instance.ShowMyAttackers(atkZone.GetZoneElements());
        for (int i=0;i< placersMine.Count;i++) {
            if (placersMine[i].Slot != null) {
                Destroy(placersMine[i].Slot.gameObject);
            }
        }
    }

    public void InstantiateOponentCards(List<CardPosition> cardsPosition) {
        StartCoroutine(InstantiateOponentCardsRotine(cardsPosition));
    }

    IEnumerator InstantiateOponentCardsRotine(List<CardPosition> cardsPosition) {
        yield return new WaitForSeconds(5f);
        WaitForEndOfFrame w8 = new WaitForEndOfFrame();
        for (int i = 0; i < cardsPosition.Count; i++)
        {
            Debug.Log(cardsPosition[i].Card.id);
            GameObject newCardOponent = Instantiate(Resources.Load("Prefabs/CardOponent")) as GameObject;
            CardOponent cardOponent = newCardOponent.GetComponent<CardOponent>();
            oponentCardsInBoard.Add(cardOponent);
            cardOponent.Initialize(cardsPosition[i].Card);
            placersRival[cardsPosition[i].Position].PutInZone(cardOponent);
            yield return w8;
        }
    }

}
