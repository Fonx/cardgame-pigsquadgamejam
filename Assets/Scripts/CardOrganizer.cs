using System.Collections.Generic;
using UnityEngine;

public class CardOrganizer : MonoBehaviour
{
    public static CardOrganizer _instance;
    public List<Transform> cardTransforms;
    [SerializeField] List<Transform> transformToGo = new List<Transform>();

    public float cardWidth = 1f; // width of the card
    public float spacing = 0.2f; // spacing between cards
    public float yOffset = 0f; // vertical offset of cards
    public float xOffset = 0f; // horizontal offset of cards

    public float cardAngle = 2f;
    private float startAngle; // initial angle of first card
    private float endAngle; // final angle of last card

    public float factor;

    public float maxDistanceFromCenter = .5f;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(gameObject);
    }
    private void Start()
    {
        OrganizeCards();
    }

    public void OrganizeCards()
    {
        Debug.Log("aaaa.    ");
        cardTransforms.RemoveAll(e => e == null);
        Debug.Log("organizando..");
        for (int i = 0; i < cardTransforms.Count; i++)
        {
            cardTransforms[i].position = transformToGo[i].position;
        }
    }

    private Vector3 CalculateBezierPoint(float t, Vector3[] controlPoints)
    {
        if (controlPoints.Length == 4)
        {
            Vector3 p0 = controlPoints[0];
            Vector3 p1 = controlPoints[1];
            Vector3 p2 = controlPoints[2];
            Vector3 p3 = controlPoints[3];

            float u = 1f - t;
            float tt = t * t;
            float uu = u * u;
            float uuu = uu * u;
            float ttt = tt * t;

            Vector3 p = uuu * p0;
            p += 2f * uu * t * p1;
            p += 2f * u * tt * p2;
            p += ttt * p3;

            return p;
        }
        else
        {
            Debug.LogError("Invalid number of control points for Bezier curve calculation. Expected 4 points.");
            return Vector3.zero;
        }
    }

    public void AddCardToHand(CardSO card)
    {
        GameObject newCard = Instantiate(Resources.Load("Prefabs/CardHand"), transform) as GameObject;
        newCard.GetComponent<CardHand>().Initialize(card);
        cardTransforms.Add(newCard.transform);

        OrganizeCards();
    }
}