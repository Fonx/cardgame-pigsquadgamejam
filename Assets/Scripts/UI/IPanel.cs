using System.Collections;
using UnityEngine;

public class IPanel : MonoBehaviour
{
    private bool inRotine;
    private void OnEnable()
    {
        if (inRotine)
        {
            StopAllCoroutines();
        }
        StartCoroutine(Open());
    }

    public void ClosePanel()
    {
        if (inRotine)
        {
            StopAllCoroutines();
        }
        StartCoroutine(Close());
    }

    IEnumerator Close()
    {
        Vector3 finalPos = new Vector3(transform.localScale.x, 0, transform.localScale.z);
        while (transform.localScale != finalPos)
        {
            transform.localScale = Vector3.MoveTowards(transform.localScale, finalPos, 0.4f);
            yield return new WaitForSeconds(0.05f);
        }
        gameObject.SetActive(false);
    }
    IEnumerator Open()
    {
        while (transform.localScale != Vector3.one)
        {
            transform.localScale = Vector3.MoveTowards(transform.localScale, Vector3.one, 0.4f);
            yield return new WaitForSeconds(0.05f);
        }
    }
}
