using UnityEngine;

public class Damageble : MonoBehaviour
{
    [SerializeField]private int counter;

    //public int Counter { get => counter; set => counter = value; }

    private Card cardOwner;
    void OnTriggerEnter(Collider other)
    {
        Debug.Log("colidiu");
        if (other.CompareTag("Card")) {
            //yield return new WaitForSeconds(.2f);
            Card target = other.GetComponent<Card>();
            if(target != cardOwner)
                other.GetComponent<Card>().PutCounter(counter);
        }
    }

    public void Initialize(Card owner, int counter) {
        this.counter = counter;
        this.cardOwner = owner;
    }
}
