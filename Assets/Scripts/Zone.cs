using System.Collections.Generic;
using UnityEngine;

public class Zone : MonoBehaviour
{
    [SerializeField] List<CardPlacer> placers = new List<CardPlacer>();

    [SerializeField] List<CardSO> cards = new List<CardSO>();

    [SerializeField] CardZone thisZone;

    public CardZone ThisZone { get => thisZone; set => thisZone = value; }
    public List<CardPlacer> Placers { get => placers; set => placers = value; }

    public void RemoveCard(CardSO card) {
        cards.Remove(card);
        cards.RemoveAll(e => e == null);
    }
    public void AddCard(CardSO card, CardPlacer placer) {
        cards.Add(card);
    }

    public CardPlacer GetNonUsedSpace() {
        for (int i=0;i<placers.Count;i++) {
            if (placers[i].Slot == null) {
                return placers[i];
            }
        }
        Debug.Log("n achou");
        return null;
    }

    public void PlayCardAt(CardSO playedCard)
    {
        CardPlacer placer = GetNonUsedSpace();
        if (placer == null) {
            Debug.Log("n tem espa�o!!");
        }
        else {
            GameObject instantiatedCard = Instantiate(Resources.Load("Prefabs/CardScene")) as GameObject;
            CardScene newCard = instantiatedCard.GetComponent<CardScene>();
            newCard.Initialize(playedCard);
            placer.PutInZone(newCard);
        }
    }

    public List<CardPosition> GetZoneElements() {
        List <CardPosition> elements = new List<CardPosition>();
        for (int i=0;i<placers.Count;i++) {
            if (placers[i].Slot != null) {
                elements.Add(new CardPosition(placers[i].Slot.Reference, i));
            }
        }
        return elements;
    }
}
