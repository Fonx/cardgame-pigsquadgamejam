using UnityEngine;

public class CardPlacer : MonoBehaviour
{
    [SerializeField] Zone thisZone;
    [SerializeField]private Card slot;

    public Zone ThisZone { get => thisZone; }
    public CardZone ThisZoneCode { get => thisZone.ThisZone; }
    public Card Slot { get => slot; set => slot = value; }
    public CardPlacer Next { get => next; set => next = value; }

    [SerializeField] CardPlacer next;

    public void PutInZone(Card cardToPut) {
        Debug.Log(cardToPut.Reference.id);
        if (cardToPut.CurrentPlacer != null) {
            cardToPut.CurrentPlacer.RemoveFromZone(cardToPut);
        }
        slot = cardToPut;
        cardToPut.transform.SetParent(transform);
        cardToPut.transform.localPosition = Vector3.zero;
        cardToPut.transform.localScale = Vector3.one;
        cardToPut.CurrentPlacer = this;
        if(ThisZone != null)
            thisZone.AddCard(cardToPut.Reference, this);
    }

    public void RemoveFromZone(Card cardToRemove) {
        slot = null;
        if (ThisZone != null)
            thisZone.RemoveCard(cardToRemove.Reference);
    }
}
