using System;
using UnityEngine;

public class CardScene : Card, IDraggable
{
    private bool isDragging = false;
    private Camera mainCamera;
    private Collider col;


    private CardZone currentZone;


    private bool blocked = false;

    protected override void Start()
    {
        base.Start();
        currentZone = CardZone.MainZone;
        col = GetComponent<Collider>();
        mainCamera = Camera.main;
        CameraRaycaster cameraRaycaster = mainCamera.GetComponent<CameraRaycaster>();
        GameManager gameManager = GameManager._instance;

        cameraRaycaster.onMouseDown += MouseDown;
        cameraRaycaster.onMouseUp += MouseUp;
        gameManager.onChoosingCardToDraw += Freeze;
        gameManager.onDrawCard += Unfreeze;
        NetworkSubscriber._instance.onPhaseChange += OnChangePhase;
    }

    private void Freeze() { blocked = true; }
    private void Unfreeze() { blocked = false; }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        mainCamera = Camera.main;
        CameraRaycaster cameraRaycaster = mainCamera.GetComponent<CameraRaycaster>();

        cameraRaycaster.onMouseDown -= MouseDown;
        cameraRaycaster.onMouseUp -= MouseUp;
        GameManager._instance.onChoosingCardToDraw -= Freeze;
        NetworkSubscriber._instance.onPhaseChange -= OnChangePhase;
    }

    void Update()
    {
        if (isDragging)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                //Debug.Log("draging");
                transform.position = new Vector3(hit.point.x, hit.point.y,12);
            }
        }
    }

    #region MOVE
    public void OnStartDrag()
    {
        isDragging = true;
        col.enabled = false;
        //startPosition = transform.position;
    }

    public void OnEndDrag(Transform content)
    {
        transform.SetParent(content);
        currentZone = content.GetComponent<CardPlacer>().ThisZoneCode;
        Release();
        Invoke("PlayCardClip", .5f);
        //onCardPlaced.Invoke(content);
    }

    private void Release()
    {
        isDragging = false;
        col.enabled = true;
        transform.localPosition = Vector3.zero;
        transform.rotation = Quaternion.identity;
        audioSource.PlayOneShot(putCardClip);
    }

    void MouseDown(GameObject obj)
    {
        if (obj == gameObject && blocked == false)
        {
            OnStartDrag();
        }
    }

    void MouseUp(CardPlacer placer, GameObject obj)
    {
        if (placer != null && obj == gameObject)
        {
            //placer.ThisZone.AddCard(reference, placer);
            placer.PutInZone(obj.GetComponent<CardScene>());
            OnEndDrag(placer.transform);
        }
        else if (placer == null && obj == gameObject)
        {
            //Debug.Break();
            Release();
            Camera.main.GetComponent<CameraRaycaster>().CurrentDrag = null;
            Debug.Log("placer nulo");
        }
    }
    #endregion


    #region PHASE
    private void OnChangePhase(int phase)
    {
        if (phase == 0) {
            int count = 0;
            for (int i=0;i<reference.collectskills.Count;i++) {
                for (int j=0;j<reference.collectQtd[i];j++) {
                    count++;
                    //Debug.Log(Enum.GetName(typeof(SkillCode), (int) reference.skillsBase[i].code));
                    Invoke(reference.collectskills[i].code.ToString(), count);
                }
            }

        }
    }


    #endregion


    #region SkillCodes
    private void StoneHarvest()
    {
        if (currentZone != CardZone.EnvorimentZone)
            return;
        // Deduct X stones from the player
        GameManager._instance.Stone +=1;
        Instantiate(Resources.Load("Prefabs/Effects/GainStone"), transform);
        Debug.Log("Earning X stones from the player.");
    }

    private void GoldBounty()
    {
        if (currentZone != CardZone.EnvorimentZone)
            return;
        // Earn X frogs for the player
        GameManager._instance.Gold += 1;
        if (currentZone == CardZone.EnvorimentZone)
        {
            Instantiate(Resources.Load("Prefabs/Effects/GainGold"), transform);
        }

        Debug.Log("Earning X frogs for the player.");
    }

    private void Woodcutter()
    {
        if (currentZone != CardZone.EnvorimentZone)
            return;
        // Earn X woods for the player
        GameManager._instance.Wood += 1;
        Instantiate(Resources.Load("Prefabs/Effects/GainWood"), transform);
        Debug.Log("Earning X woods for the player.");
    }

    private void BunnyHunter()
    {
        if (currentZone != CardZone.EnvorimentZone)
            return;
        // Earn X rabbits for the player
        GameManager._instance.Rabbit += 1;
        Instantiate(Resources.Load("Prefabs/Effects/GainRabbit"), transform);
        Debug.Log("Earning X rabbits for the player.");
    }

    private void WoodTax()
    {
        // Deduct X woods from the player
        GameManager._instance.Wood -= 1;
        Instantiate(Resources.Load("Prefabs/Effects/GainWood"), transform);
        Debug.Log("Deducting X woods from the player.");
    }

    private void StoneLevy()
    {
        // Deduct X stones from the player
        GameManager._instance.Stone -= 1;
        Instantiate(Resources.Load("Prefabs/Effects/GainStone"), transform);
        Debug.Log("Deducting X stones from the player.");
    }

    private void GoldDrain()
    {
        // Deduct X frogs from the player
        GameManager._instance.Gold -= 1;
        Instantiate(Resources.Load("Prefabs/Effects/GainGold"), transform);
        Debug.Log("Deducting X frogs from the player.");
    }

    private void RabbitRansom()
    {
        // Deduct X rabbits from the player
        GameManager._instance.Rabbit -= 1;
        Instantiate(Resources.Load("Prefabs/Effects/GainRabbit"), transform);
        Debug.Log("Deducting X rabbits from the player.");
    }

    private void Proliferate()
    {
        if (currentZone != CardZone.MainZone)
            return;
        
        Instantiate(Resources.Load("Prefabs/Effects/Proliferate"), transform);
        Debug.Log("Proliferate.");
    }

    #endregion


    public override void PutCounter(int qtd)
    {
        counter += qtd;
        if (counter >= reference.Status.y)
        {
            DestroyCard();
        }
        counterBorder.SetActive(true);
        counterText.text = counter.ToString();
    }

    private void onStartPhaseBegins(int phase)
    {
        if (phase == 0)
        {
            counterBorder.SetActive(false);
            counter = 0;
        }
    }

    private void PlayCardClip() {
        audioSource.PlayOneShot(reference.clip);
    }


}
public enum SkillCode { StoneHarvest, GoldBounty, Woodcutter, BunnyHunter, WoodTax, StoneLevy, GoldDrain, RabbitRansom }