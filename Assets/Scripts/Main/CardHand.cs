using System.Collections.Generic;
using UnityEngine;

public class CardHand : Card
{
    [SerializeField] private List<SpriteRenderer> renderers = new List<SpriteRenderer>();
    private bool selected = false;
    private bool used = false;
    protected override void Start()
    {
        base.Start();
        InitializeValue();
        CameraRaycaster cameraRaycaster = Camera.main.GetComponent<CameraRaycaster>();
        cameraRaycaster.onMouseDown += MouseDown;
        cameraRaycaster.onMouseHover += MouseHover;
        cameraRaycaster.onMouseEndHover += MouseEndHover;
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        CameraRaycaster cameraRaycaster = Camera.main.GetComponent<CameraRaycaster>();
        cameraRaycaster.onMouseDown -= MouseDown;
        cameraRaycaster.onMouseHover -= MouseHover;
        cameraRaycaster.onMouseEndHover -= MouseEndHover;
    }
    private void UseCard() {
        Debug.Log("trying to use card...");
        if (GameManager._instance.AbleToPay(reference)) {
            used = true;
            //StartCoroutine(UseCardRotine());
            GameManager._instance.PlayCard(this);
        }
    }
    private void MouseDown(GameObject obj)
    {
        if (used) {
            return;
        }
        if (obj == gameObject) {
            UseCard();
        }
    }
    void MouseHover(Card card)
    {
        MouseEndHover();
        if (card == this && selected == false) {
            selected = true;
            transform.localScale = Vector3.one * 1.4f;
            audioSource.PlayOneShot(putCardClip);
            for (int i=0;i<renderers.Count;i++) {
                renderers[i].sortingOrder = renderers[i].sortingOrder + 10;
            }
        }
    }
    void MouseEndHover()
    {
        transform.localScale = Vector3.one;
        if (selected) {
            for (int i = 0; i < renderers.Count; i++)
            {
                renderers[i].sortingOrder = renderers[i].sortingOrder - 10;
            }
        }
        selected = false;
    }
}
