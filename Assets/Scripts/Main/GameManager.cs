using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

public class GameManager : MonoBehaviour
{
    [SerializeField] Transform deckContent;
    [SerializeField] List<Zone> zones;

    public string playerName;
    public string oponentName;

    private List<int> resources = new List<int>(4) { 1,1,1,1};
    private List<int> resourcesMultiply = new List<int>(4) { 1,1,1,1};


    private Stack<CardDeck> deck = new Stack<CardDeck>();
    private Stack<CardSO> cardsToDraw = new Stack<CardSO>();
    private List<GameObject> cardsToShow = new List<GameObject>(3) { };

    private List<CardSO> dictionary = new List<CardSO>();

    public static GameManager _instance;
    private bool isGameStarted;

    public event System.Action<bool> pause = delegate { };
   
    public int Wood { get => resources[0]; set { resources[0] = value * resourcesMultiply[0]; OnResourceChange.Invoke(resources); } }
    public int Stone { get => resources[1]; set { resources[1] = value * resourcesMultiply[1]; OnResourceChange.Invoke(resources); } }
    public int Rabbit { get => resources[2]; set { resources[2] = value * resourcesMultiply[2]; OnResourceChange.Invoke(resources); } }
    public int Gold { get => resources[3]; set { resources[3] = value * resourcesMultiply[3]; OnResourceChange.Invoke(resources); } }

    private bool inRotine;
    public event Action<List<int>> OnResourceChange = delegate { };

    public event Action onChoosingCardToDraw = delegate { };
    public event Action onDrawCard = delegate { };
    public event Action<int> onPlayCard = delegate { };
    public event Action<int> onUpgradeDeck = delegate { };

    [SerializeField] GameObject win;
    [SerializeField] GameObject lose;

    private int currentDeckLevel;
    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(gameObject);

        Application.targetFrameRate = 120;
        GenerateDictionary();
        GenerateDeckInitial();
        Application.targetFrameRate = 60;
    }

    public bool PlayCard(Card card) {
        if (AbleToPay(card.Reference))
        {
            StartCoroutine(PlayCardRotine(card));
            return true;
        }
        else {
            return false;
        }
    }
    IEnumerator PlayCardRotine(Card card) {
        inRotine = true;
        onChoosingCardToDraw.Invoke();
        WaitForSeconds w8 = new WaitForSeconds(1 / 60f);

        RetireResources(card.Reference);
        PlayCardAnimation(card.Reference);
        Destroy(card.gameObject);
        yield return w8;
        if (card.Reference.category == CardCategory.Magic)
        {
            NetworkSubscriber._instance.PlayMagicCard(card.Reference.keycode, card.Reference.id);
        }
        else if (card.Reference.category == CardCategory.Exchange) {
            Exchange(Helper.MapCode(card.Reference.collectskills), card.Reference.collectQtd);
        }
        else if (card.Reference.category == CardCategory.Expansion) {
            AddUpgradeOnDeck();
        }
        else
        {
            zones[0].PlayCardAt(card.Reference);
        }

        yield return w8;
        CardOrganizer._instance.OrganizeCards();
        yield return new WaitForSeconds(.4f);
        ShowAndChooseNewCard();
    }

    private void Exchange(List<SkillCode> skills, List<int> qtd) {
        for (int i=0;i<skills.Count;i++) {
            switch (skills[i])
            {
                case SkillCode.BunnyHunter:
                    GameObject a = Instantiate(Resources.Load("Prefabs/Effects/GainRabbit"), transform) as GameObject;
                    a.transform.localScale = Vector3.one *2;
                    Rabbit += qtd[i];
                    break;
                case SkillCode.GoldBounty:
                    GameObject b = Instantiate(Resources.Load("Prefabs/Effects/GainGold"), transform) as GameObject;
                    b.transform.localScale = Vector3.one*2;
                    Gold += qtd[i];
                    break;
                case SkillCode.Woodcutter:
                    GameObject c = Instantiate(Resources.Load("Prefabs/Effects/GainWood"), transform) as GameObject;
                    c.transform.localScale = Vector3.one*2;
                    Wood += qtd[i];
                    break;
                case SkillCode.StoneHarvest:
                    GameObject d = Instantiate(Resources.Load("Prefabs/Effects/GainStone"), transform) as GameObject;
                    d.transform.localScale = Vector3.one*2;
                    Stone += qtd[i];
                    break;
            }
        }

    }

    private void PlayCardAnimation(CardSO card) {
        onPlayCard.Invoke(card.id);
    }
    public void PlayCardAnimation(int cardId) {
        CardSO card = GetCard(cardId);
        onPlayCard.Invoke(card.id);
    }

    private void RetireResources(CardSO card) {
        for (int i = 0; i < card.costResources.Count; i++)
        {
            resources[(int)card.costResources[i]] -= card.costQtd[i];
        }
        OnResourceChange.Invoke(resources);
    }

    public bool AbleToPay(CardSO card)
    {
        if (!AbleToPayCost(card))
        {
            ToolTip._instance.ShowMessage("You dont have enough resources to play this card.");
            return false;
        }
        else if (zones[0].GetNonUsedSpace() == null)
        {
            ToolTip._instance.ShowMessage("Your zone does not have more spaces.");
            return false;
        }
        else if (NetworkSubscriber._instance.CurrentPhase == 2) {
            ToolTip._instance.ShowMessage("Can't play cards in the attack phase.");
            return false;
        }
        else if (inRotine) {
            return false;
        }
        else {
            return true;
        }
    }

    private bool AbleToPayCost(CardSO card) {
        for (int i = 0; i < card.costResources.Count; i++)
        {
            if (resources[(int)card.costResources[i]] < card.costQtd[i])
            {
                return false;
            }
        }
        return true;
    }

    private void GenerateDeckInitial()
    {
        Object[] scriptables = Resources.LoadAll("Scriptables/Cards/Initial", typeof(CardSO));
        Debug.Log(scriptables.Length);
        Object[] shuffledObject = Helper.Shuffle(scriptables);

        Vector3 currentOffset = Vector3.zero;
        Debug.Log(shuffledObject.Length);
        for (int i=0; i<shuffledObject.Length;i++) {
            GenerateCardOnDeck((CardSO)shuffledObject[i], currentOffset);
            currentOffset += Vector3.one * 0.002f;
        }
        Debug.Log(cardsToDraw.Count);
    }

    public CardSO GetCard(int id) {
        CardSO card = dictionary.Find(e => e.id == id);
        Debug.Log(card);
        return card;
    }

    private void GenerateDictionary()
    {
        Object[] scriptablesInitial = Resources.LoadAll("Scriptables/Cards/Initial", typeof(CardSO));
        for (int i = 0; i < scriptablesInitial.Length; i++)
        {
            dictionary.Add((CardSO)scriptablesInitial[i]);
        }

        Object[] scriptablesMedium = Resources.LoadAll("Scriptables/Cards/Medium", typeof(CardSO));
        for (int i = 0; i < scriptablesMedium.Length; i++)
        {
            dictionary.Add((CardSO)scriptablesMedium[i]);
        }

        Object[] scriptablesGreat = Resources.LoadAll("Scriptables/Cards/Great", typeof(CardSO));
        for (int i = 0; i < scriptablesGreat.Length; i++)
        {
            dictionary.Add((CardSO)scriptablesGreat[i]);
        }

        Object[] scriptablesMaster = Resources.LoadAll("Scriptables/Cards/Master", typeof(CardSO));
        for (int i = 0; i < scriptablesMaster.Length; i++)
        {
            dictionary.Add((CardSO)scriptablesMaster[i]);
        }


        dictionary.Sort((t1, t2) => (t1.id > t2.id) ? 1 : -1);
    }

    private void ShowAndChooseNewCard() {
        Debug.Log("ChooseAndDrawNewCard");
        Vector3 xOffset = new Vector3(-10,-7f,8);
        for (int i=0;i<3;i++) {
            CardSO onTopCard = cardsToDraw.Pop();
            CardDeck thisCardShape = deck.Pop();
            thisCardShape.onChooseCard += ChooseNewCard;
            cardsToShow.Add(thisCardShape.gameObject);

            thisCardShape.transform.position = xOffset;
            thisCardShape.transform.rotation = Quaternion.identity;
            xOffset += Vector3.right * 10;
            thisCardShape.Initialize(onTopCard);
        }
    }

    public void AddUpgradeOnDeck() {
        currentDeckLevel++;
        onUpgradeDeck.Invoke(currentDeckLevel);
        switch (currentDeckLevel)
        {
            case 1:
                UpgradeDeck("Medium");
                break;
            case 2:
                UpgradeDeck("Great");
                break;
            case 3:
                UpgradeDeck("Master");
                break;
        }
    }
    private void UpgradeDeck(string input)
    {
        cardsToDraw = new Stack<CardSO>();
        Object[] scriptables = Resources.LoadAll($"Scriptables/Cards/{input}", typeof(CardSO));
        Object[] shuffledObject = Helper.Shuffle(scriptables);

        for (int i = 0; i < shuffledObject.Length; i++)
        {
            cardsToDraw.Push((CardSO)shuffledObject[i]);
        }
    }

    private void ChooseNewCard(CardSO card) {
        for (int i=0;i<cardsToShow.Count;i++) {
            if (cardsToShow[i] != null) {
                Destroy(cardsToShow[i]);
            }
        }
        cardsToShow = new List<GameObject>(3);

        CardOrganizer._instance.AddCardToHand(card);
        onDrawCard.Invoke();
        inRotine = false;
    }

    private void GenerateCardOnDeck(CardSO currentCard, Vector3 currentOffset)
    {
        cardsToDraw.Push(currentCard);
        GameObject thisObj = Instantiate(Resources.Load("Prefabs/CardDeck"), deckContent) as GameObject;
        thisObj.transform.localPosition += currentOffset;
        thisObj.transform.rotation = Quaternion.Euler(0, 180, 0);
        deck.Push(thisObj.GetComponent<CardDeck>());
    }

    public CardPlacer GetRandomAlly() {
        for (int i=0;i<zones.Count;i++)
        {
            for (int j=0;j<zones[i].Placers.Count;j++)
            {
                if (zones[i].Placers[j].Slot != null) {
                    return zones[i].Placers[j];
                }
            }
        }
        return null;
    }

    public void PauseGame() {
        isGameStarted = false;
        //Time.timeScale = 0;
        pause.Invoke(true);
        Debug.Log("PauseGame");
    }

    public void ResumeGame() {
        isGameStarted = true;
        //Time.timeScale = 1;
        pause.Invoke(false);
    }

    public void LoseGame() {
        lose.SetActive(true);
        NetworkSubscriber._instance.Lose();
    }
    public void WinGame() {
        win.SetActive(true);
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void Close() {
        SceneManager.LoadScene("Lobby");
    }
}
