using System;
using UnityEngine;

public class CardDeck : Card
{
    public event Action<CardSO> onChooseCard = delegate{};
    [SerializeField] GameObject effectOutline;
    protected override void Start()
    {
        base.Start();
        InitializeValue();
    }

    private void OnDestroy()
    {
        CameraRaycaster cameraRaycaster = Camera.main.GetComponent<CameraRaycaster>();
        cameraRaycaster.onMouseDown -= OnChooseCard;
        cameraRaycaster.onChooserHover -= MouseHover;
        cameraRaycaster.onMouseEndHover -= MouseEndHover;
    }

    public void ChooseCard() {
        onChooseCard(reference);
    }

    public void OnChooseCard(GameObject card) {
        if (card == gameObject) {
            ChooseCard();
            Debug.Log("selecionando card...");
        }
    }

    public new void Initialize(CardSO reference)
    {
        base.Initialize(reference);
        CameraRaycaster cameraRaycaster = Camera.main.GetComponent<CameraRaycaster>();
        cameraRaycaster.onMouseDown += OnChooseCard;
        cameraRaycaster.onChooserHover += MouseHover;
        cameraRaycaster.onMouseEndHover += MouseEndHover;
    }



    void MouseHover(Card card)
    {
        if (card == this) {
            effectOutline.SetActive(true);
        }
    }
    void MouseEndHover()
    {
        effectOutline.SetActive(false);
    }
}
