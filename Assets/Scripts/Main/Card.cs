using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Card : SubscriberSound
{
    [SerializeField] protected CardSO reference;
    [SerializeField] protected SpriteRenderer cardImage;
    [SerializeField] protected SpriteRenderer cardBackImage;
    [SerializeField] protected TMP_Text cardTitle;
    [SerializeField] protected TMP_Text cardDescription;

    [SerializeField] protected TMP_Text cardPower;
    [SerializeField] protected TMP_Text cardDefense;

    private Sprite backEnviroment;
    private Sprite backInvocation;
    private Sprite backConstruction;
    private Sprite backExpansion;
    private Sprite backMagic;

    protected Sprite backEnemy;

    [SerializeField] protected AudioClip putCardClip;

    [SerializeField] Transform costContent;
    [SerializeField] protected CardPlacer currentPlacer;
    public CardPlacer CurrentPlacer { get => currentPlacer; set => currentPlacer = value; }

    public CardSO Reference { get => reference; }


    protected int counter;
    [SerializeField]protected TMP_Text counterText;
    [SerializeField] protected GameObject counterBorder;

    [SerializeField] GameObject cardStatus;
    protected virtual void Awake()
    {
        GetSpriteBackReferences();
    }

    public void Initialize(CardSO reference)
    {
        this.reference = reference;
        InitializeValue();
    }

    protected virtual void OnDestroy()
    {
        Instantiate(Resources.Load("Prefabs/Effects/CFX_Hit_SmokePuff"), transform.position, Quaternion.identity);
    }

    protected void InitializeValue()
    {
        if (reference == null)
            return;
        cardImage.sprite = reference.sprite;
        cardBackImage.sprite = FillBackSpriteCard();
        cardTitle.text = reference.cardName;
        cardDescription.text = reference.description;

        if (reference.category == CardCategory.Invocation || reference.category == CardCategory.Construction)
        {
            cardStatus.SetActive(true);
            cardPower.text = reference.Status.x.ToString();
            cardDefense.text = reference.Status.y.ToString();
        }
        else {
            cardPower.text = "";
            cardDefense.text = "";
            cardStatus.SetActive(false);
        }
        InitializeCost();
        InitializeDescription();
    }

    private void InitializeCost() {
        for (int i=0;i<costContent.childCount;i++) {
            Destroy(costContent.GetChild(i).gameObject);
        }
        for (int i=0;i<reference.costResources.Count;i++) {
            GameObject costController = Instantiate(Resources.Load("Prefabs/CostOrganizer"), costContent) as GameObject;
            costController.GetComponent<CostOrganizer>().Initialize(reference.costResources[i], reference.costQtd[i]);
        }
    }    
    private void InitializeDescription() {
        string desc = "";
        for (int i=0;i<reference.collectskills.Count;i++) {
            desc += $"{reference.collectskills[i].skillName} {reference.collectQtd[i]} \n";
        }
        for (int i = 0; i < reference.combatskills.Count; i++){
            desc += $"{reference.combatskills[i].skillName} {reference.combatQtd[i]} \n";
        }
        cardDescription.text = desc+reference.description;
    }

    private void GetSpriteBackReferences() {
        Object[] scriptables = Resources.LoadAll("CardBackImage", typeof(Sprite));
        backEnviroment = (Sprite)scriptables[2];
        backInvocation = (Sprite)scriptables[4];
        backConstruction = (Sprite)scriptables[1];
        backExpansion = (Sprite)scriptables[3];
        backMagic = (Sprite)scriptables[5];
        //rival card
        backEnemy = (Sprite)scriptables[0];
    }

    protected virtual Sprite FillBackSpriteCard()
    {
        switch (reference.category)
        {
            case CardCategory.Construction:
                return backConstruction;
            case CardCategory.Enviroment:
                return backEnviroment;
            case CardCategory.Expansion:
                return backExpansion;
            case CardCategory.Invocation:
                return backInvocation;
            case CardCategory.Magic:
                return backMagic;
            default:
                return backMagic;
        }
    }

    public void DestroyCard()
    {
        if (currentPlacer != null)
            currentPlacer.RemoveFromZone(this);
        Destroy(gameObject);
    }

    public virtual void PutCounter(int qtd) { }
}
