using UnityEngine;
using TMPro;
public class CardOponent : Card
{
    protected override void Start()
    {
        base.Start();
        NetworkSubscriber._instance.onPhaseChange += onStartPhaseBegins;
    }

    protected override Sprite FillBackSpriteCard()
    {
        return backEnemy;
    }

    public override void PutCounter(int qtd) {
        counter += qtd;
        if (counter >= reference.Status.y)
        {
            DestroyCard();
        }
        counterBorder.SetActive(true);
        counterText.text = counter.ToString();
    }

    private void onStartPhaseBegins(int phase) {
        if (phase == 0) {
            counterBorder.SetActive(false);
            counter = 0;
        }
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        NetworkSubscriber._instance.onPhaseChange -= onStartPhaseBegins;
    }


}
