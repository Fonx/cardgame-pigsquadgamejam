using UnityEngine;
using TMPro;

public class SkillCardTip : MonoBehaviour
{
    [SerializeField] TMP_Text title;
    [SerializeField] TextMeshProUGUI description;

    public void Initialize(string title, string description) {

        //title += '\u29EB';
        //byte[] bytes = Encoding.Default.GetBytes(description);
        this.title.text = title;
        this.description.text = description;
    }
}
