using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResourcesObserver : MonoBehaviour
{
    [SerializeField] TMP_Text wood;
    [SerializeField] TMP_Text stone;
    [SerializeField] TMP_Text rabbit;
    [SerializeField] TMP_Text frog;
    void Start()
    {
        GameManager manager = GameManager._instance;
        manager.OnResourceChange += UpdateCanvas;
        UpdateCanvas(new List<int>() { manager.Wood, manager.Stone, manager.Rabbit, manager.Gold });
    }

    private void UpdateCanvas(List<int> resources) {
        wood.text = resources[0].ToString();
        stone.text = resources[1].ToString();
        rabbit.text = resources[2].ToString();
        frog.text = resources[3].ToString();
    }

}
