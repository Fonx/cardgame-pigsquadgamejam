using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LayoutPlayerObserver : SubscriberSound
{
    [SerializeField] TMP_Text rivalName;
    [SerializeField] TMP_Text playerName;

    [SerializeField] List<Image> phases;

    private Color32 selectedColor = new Color32(163,0,255,255);
    //private Color selectedColor = Color.red;
    private Color unSelectedColor = Color.white;

    public TMP_Text currentPhaseText;

    public AudioClip clip;
    void Awake()
    {
        NetworkSubscriber network = NetworkSubscriber._instance;
        network.onLocalPlayerConnect += OnPlayerConnect;
        network.onRivalPlayerConnect += OnRivalConnect;
        network.onPhaseChange += OnChangePhase;
    }

    private void OnRivalConnect(Player player) {
        rivalName.text = player.NickName;
    }

    private void OnPlayerConnect(Player player) {
        playerName.text = player.NickName;
    }

    private void OnChangePhase(int phase) {
        StartCoroutine(ChangePhaseRotine(phase));
    }

    IEnumerator ChangePhaseRotine(int phase)
    {
        WaitForSeconds w8 = new WaitForSeconds(1 / 60);
        float t = 0;
        audioSource.PlayOneShot(clip);
        switch (phase)
        {
            case 0:
                currentPhaseText.text = "Upkeep";
                break;
            case 1:
                currentPhaseText.text = "Management";
                break;
            case 2:
                currentPhaseText.text = "Combat";
                break;
        }
        while (phases[phase].color != selectedColor) {
            t += 1 / 60f;
            for (int i=0;i<phases.Count;i++) {
                currentPhaseText.transform.localScale = Vector3.one * t;
                if (i == phase)
                {
                    phases[phase].color = Color32.Lerp(phases[phase].color, selectedColor, t);
                    yield return w8;
                }
                else {
                    phases[i].color = Color32.Lerp(phases[i].color, unSelectedColor, t);
                    yield return w8;
                }
            }
        }
        yield return new WaitForSeconds(1f);
        currentPhaseText.transform.localScale = Vector3.zero;
    }

}
