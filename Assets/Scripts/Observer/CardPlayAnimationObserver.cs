using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardPlayAnimationObserver : MonoBehaviour
{
    [SerializeField] Card card;
    private Vector3 startPos;
    private Vector3 finalPos;

    void Start()
    {
        GameManager._instance.onPlayCard += OnPlayCard;
        startPos = card.transform.position;
        finalPos = startPos + Vector3.forward * -30f;
    }

    private void OnPlayCard(int cardId) {
        card.gameObject.SetActive(true);
        CardSO thisCard = GameManager._instance.GetCard(cardId);
        card.Initialize(thisCard);
        card.transform.position = startPos;
        StartCoroutine(MoveRotine());
    }

    IEnumerator MoveRotine() {
        WaitForSeconds w8 = new WaitForSeconds(1 / 60);
        while (card.transform.position != finalPos) {
            yield return w8;
            card.transform.position = Vector3.MoveTowards(card.transform.position, finalPos, Time.deltaTime * 100f);
        }
        card.gameObject.SetActive(false);
    }


}
