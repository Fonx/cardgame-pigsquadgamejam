using System.Collections.Generic;
using UnityEngine;

public class ShowCard : MonoBehaviour
{
    [SerializeField] Card filmedCard;
    [SerializeField] GameObject imageCanvas;

    [SerializeField] List<SkillCardTip> skillTips = new List<SkillCardTip>();

    private bool blocked = false;

    private void Start()
    {
        CameraRaycaster subscriber = Camera.main.GetComponent<CameraRaycaster>();
        GameManager gameManager = GameManager._instance;

        subscriber.onMouseHover += StartAndShowCard;
        subscriber.onMouseEndHover += HideCard;
        subscriber.onHouveBack += HouveBack;


        gameManager.onChoosingCardToDraw += Freeze;
        gameManager.onDrawCard += Unfreeze;
    }
    public void StartAndShowCard(Card card) {
        if (blocked)
        {
            return;
        }
        imageCanvas.SetActive(true);

        //Debug.Log(card.gameObject.name);
        filmedCard.Initialize(card.Reference);
        filmedCard.transform.rotation = Quaternion.identity;
        FillSkillsTip(card.Reference);
    }

    private void FillSkillsTip(CardSO card) {
        for (int k=0;k<skillTips.Count;k++) {
            skillTips[k].gameObject.SetActive(false);
        }
        int i;
        for (i=0;i<card.collectskills.Count;i++) {
            skillTips[i].gameObject.SetActive(true);
            skillTips[i].Initialize(card.collectskills[i].skillName, card.collectskills[i].description);
        }
        for (int j = i; j < card.combatskills.Count; j++) {
            skillTips[j].gameObject.SetActive(true);
            skillTips[j].Initialize(card.combatskills[j].skillName, card.combatskills[j].description);
        }
    }

    private void HouveBack() {
        if (blocked) {
            return;
        }
        imageCanvas.SetActive(true);
        filmedCard.transform.rotation = Quaternion.Euler(0, 180, 0);
    }

    private void HideCard()
    {
        imageCanvas.SetActive(false);
    }

    public void OnCardToPickSelected() {
        HideCard();
    }

    private void Freeze() { blocked = true; }
    private void Unfreeze() { blocked = false; }
}
