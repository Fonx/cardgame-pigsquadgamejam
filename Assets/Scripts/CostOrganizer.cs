using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CostOrganizer : MonoBehaviour
{
    public TMP_Text text;
    public Image img;

    public List<Sprite> resourcesSprite;

    public void Initialize(CardResource costResources, int costQtd) {
        img.sprite = resourcesSprite[(int)costResources];
        text.text = costQtd.ToString();
    }
}
